package com.suning.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.suning.cache.UserCache;
import com.suning.utils.JsonUtils;
/**
 * Index控制器
 * @author 17010651
 *
 */
@Controller
public class IndexController {

    @Autowired
    private UserCache userCache;

    /**
     * 获取所有User
     * @param model
     * @return
     */
    @RequestMapping("/users")
    public String users(Model model) {
        model.addAttribute("users",JsonUtils.parseJSON(userCache.getUsers()));
        return "index";
    }
}
