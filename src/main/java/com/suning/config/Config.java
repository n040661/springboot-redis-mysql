package com.suning.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
/**
 * 配置类
 * @author 17010651
 */
@Configuration
public class Config {
	/**
	 * 防止返回json的时候IE变为文件下载
	 * @return
	 */
	@Bean
	public MappingJackson2HttpMessageConverter getMappingJacksonHttpMessageConverter() {
		MappingJackson2HttpMessageConverter mjtmc = new MappingJackson2HttpMessageConverter();
		List<MediaType> supportedMediaTypes = Arrays.asList(new MediaType[] { MediaType.TEXT_PLAIN });
		mjtmc.setSupportedMediaTypes(supportedMediaTypes);
		return mjtmc;
	}

	/**
	 * redisTemplate配置
	 * @param redisFactory
	 * @return
	 */
	@Bean
	public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory redisFactory) {
		return new StringRedisTemplate(redisFactory);
	}

}
