package com.suning.cache;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.suning.dao.UserMapper;
import com.suning.model.User;
import com.suning.utils.JsonUtils;

/**
 * User缓存值Redis类
 * 
 * @author 17010651
 *
 */
@Component
public class UserCache implements InitializingBean {

	private static final String USERS = "users";

	private static final Logger LOGGER = LoggerFactory.getLogger(UserCache.class);

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	/**
	 * 从缓存中获取所有User，缓存中没有则从数据库中获取并刷新缓存
	 * 
	 * @return
	 */
	public List<User> getUsers() {
		ValueOperations<String, String> opsForValue = redisTemplate.opsForValue();
		String string = opsForValue.get(USERS);
		if (StringUtils.isEmpty(string)) {
			LOGGER.info("缓存中不存在users数据，刷新缓存。");
			return refreshUsers();
		}
		return JsonUtils.parseObject(string);
	}

	/**
	 * 刷新缓存
	 * 
	 * @return
	 */
	public List<User> refreshUsers() {
		LOGGER.info("开始初始化users");
		ValueOperations<String, String> opsForValue = redisTemplate.opsForValue();
		List<User> selectByExample = userMapper.selectByExample(null);
		opsForValue.set(USERS, JsonUtils.parseJSON(selectByExample));
		LOGGER.info("初始化users完成");
		return selectByExample;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		refreshUsers();
	}

}
