package com.suning.dao;

import com.suning.model.SellOrder;
import com.suning.model.SellOrderExample;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface SellOrderMapper {
    int countByExample(SellOrderExample example);

    int deleteByExample(SellOrderExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SellOrder record);

    int insertSelective(SellOrder record);

    List<SellOrder> selectByExample(SellOrderExample example);

    SellOrder selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SellOrder record, @Param("example") SellOrderExample example);

    int updateByExample(@Param("record") SellOrder record, @Param("example") SellOrderExample example);

    int updateByPrimaryKeySelective(SellOrder record);

    int updateByPrimaryKey(SellOrder record);
}